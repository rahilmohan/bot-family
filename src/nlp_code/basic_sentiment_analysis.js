const natural = require('natural');

const sentimentFromString = (inputString) => {
    
    inputString = new natural.WordTokenizer().tokenize(inputString);
    // console.log(inputString);

    let analyzer = new natural.SentimentAnalyzer(
        "English",
        natural.PorterStemmer,
        "afinn"
    );
    
    return analyzer.getSentiment(inputString)
};

module.exports = {sentimentFromString}