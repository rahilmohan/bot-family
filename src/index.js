const twitterClient = require('./bot_code/twitter_bot');
const newsClient = require('./bot_code/newsapi_bot');
const fs = require('fs');
const sentimentAnalyzer = require('./nlp_code/basic_sentiment_analysis')
const {createObjectCsvWriter} = require('csv-writer');
const {Map, List} = require('immutable');
// const { type } = require('os');

const dotenv = require('dotenv').config({path: './config.env'});

/*
    Function to retrieve JSON containing tweets based on a keyword
 */



const getTopicWiseTweets = async() =>{
    const jsTweets = await twitterClient.roClient.v2.search('bitcoin', { 'media.fields': 'url' });
    // Consume every possible tweet of jsTweets (until rate limit is hit)
    let iter = 0;
    for await (const tweet of jsTweets) {
        console.log("Tweet:","\n",tweet);
        // console.log(tweet['text']);
        console.log("Predicted sentiment value is: ",sentimentAnalyzer.sentimentFromString(tweet['text']))
        iter+=1;
        if (iter>=4){
            break;
            }
        }
    }

/*
    Function to return JSON containing tweets from a specific user,
    signified by their numeric user ID.
    To get Twitter Nuemric ID, visit : https://tools.codeofaninja.com/find-twitter-id
    @cz_binance : 902926941413453824
*/
const getUserTweets = async() =>{
    const jsTweets = await twitterClient.roClient.v2.userTimeline('902926941413453824', { exclude: 'replies' });
    // Consume every possible tweet of jsTweets (until rate limit is hit)
    let iter = 0;
    for await (const tweet of jsTweets) {
        console.log("Tweet:","\n",tweet);
        // console.log(tweet['text']);
        console.log("Predicted sentiment value is: ",sentimentAnalyzer.sentimentFromString(tweet['text']))
        iter+=1
        if (iter>=6){
            break;
            };
        };
    };

/*
    Function to return JSON object containing news Articles dependent
    on independent parameters from the NewsApi

 */
const getNewsArticles = async(fileData) => {
    // const fileData = async() =>{
        // return data;
    // }
    // console.log("Read Next",fileData);
    const newsApiResponse = await newsClient.newsClient.v2.everything({
        q: fileData.q,
        // sources: 'bbc-news,coindesk,cointelegraph',
        // domains: 'bbc.co.uk,coindesk.com,cointelegraph.com',
        from: fileData.from,
        to: fileData.to,
        language: fileData.language,
        sortBy: fileData.sortBy,
        page: fileData.page
    })
    let iter = 0;
    console.log("Number of articles: ",newsApiResponse.totalResults)
    const resultArray = List()
    for await (const article of newsApiResponse.articles){
        // console.log("Article:","\n",article);
        // console.log(article.title);
        // console.log(sentimentAnalyzer.sentimentFromString(article.description))
        article.sentiment = sentimentAnalyzer.sentimentFromString(article.description)
        article.source = article.source.name;
        let resultArrayItem = {
            "title": article.title,
            "sentiment": sentimentAnalyzer.sentimentFromString(article.description)
        }
        // console.log(resultArrayItem.title);
        // console.log(resultArrayItem.sentiment);
        // console.log(article);
        resultArray.push(resultArrayItem);
        iter+=1;
        if (iter>=2){
            break;
            };
        };
    console.log(resultArray);
    // fs.writeFile('message.csv', JSON.stringify(resultArray), (err) => {
    //     if (err) throw err;
    //     console.log('The file has been saved!');
    //     });
    // }
    createObjectCsvWriter({
        path: `./file.csv`,
        header: [
            {id: 'title',title:'Title'},
            {id:'sentiment',title:'Sentiment'},
        ]
    })
    .writeRecords(resultArray)
    .then(()=>{
        console.log(`File created`);
    })
}

    
    // fs.writeFileSync('./newsArticle.json', JSON.stringify(resultArray), err => {
    //     if (err){
    //         console.log(e);
    //     } else {
    //         console.log('Written successfully.')
    //     };
    // })
    // return resultArray
    // };

// console.log('--------x--------------x-------------')
// console.log('Topic Wise Tweets')
// getTopicWiseTweets();
// console.log('--------x--------------x-------------')
// console.log('User Tweets from CZ')
// getUserTweets();
// console.log('--------x--------------x-------------')
// console.log('News Api Info')
// getNewsArticles()

// setInterval(getNewsArticles(), 100*60*60);

const execute = async() => {
    fs.readFile(`../config/newsapi.json`,`utf8`,(err,fileData) => {
    if (err){
        console.log(err);
        return;
    }
    fileData = JSON.parse(fileData);
    console.log("Read here",fileData);
    getNewsArticles(fileData);
    // return data;
    })
}

module.exports = {getNewsArticles,execute}