const dotenv = require('dotenv')
const news=require('newsapi')

dotenv.config({path: './config.env'});

news_api_token = process.env.NEWS_API_KEY

const newsClient = new news(`${news_api_token}`);

module.exports = {newsClient}