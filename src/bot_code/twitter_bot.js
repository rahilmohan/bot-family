const dotenv = require('dotenv');
const { TwitterApi } = require('twitter-api-v2');

dotenv.config({path: './config.env'});
bearer_token = process.env.TWITTER_BEARER_TOKEN

const userClient = new TwitterApi(`${bearer_token}`);
const roClient = userClient.readOnly
const rwClient = userClient.readWrite

module.exports = {roClient,rwClient};