const express = require('express');
const botFamily = require('./index');

const app = express();


app.get('/', (req,res) => {
    res.send("Hello to My App")
    // res.send(`${JSON.stringify(botFamily.getNewsArticles())}`)
    botFamily.execute();
    res.end();
})

const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(
    `Server started on port ${PORT}`
));