# bot-family
A series of bots for acccessing data from various api and then performing simple sentiment analysis on it using the natural.js npm package.

---
## Overview of the current available bots
1. Twiiter bot
- This module provides 2 clients, `roClient` & `rwClient`, which provide readOnly & Read Write access for Twitter.
- Requires the configuration of an environment variable `TWITTER_BEARER_TOKEN` containing the bearer token for OAuth 2.0 access.
2. NewsApi Bot
- This module provides a single client `newsClient` for retrieving data from the [News Api](https://newsapi.org/docs/endpoints/everything)
- Requires a News API key stored in the environment variable `NEWS_API_KEY`
---
## Basic Logic Flow
The logic flow from accessing the api to receiving the output is:
![Logic Flow Diagram](/assets/images/logic/LogicFlow.png)


---
## Avaliable functions
`Index.js` contains the functions which use the bots by passing certain parameters to it, elucidated below.
1. *getTopicWiseTweets()*
- Uses the `v2.search()` function from the `roClient` to call the Twitter REST api to retrive tweets containing certain keywords.
- Assuming we take the parameters, we will receive the most recent tweets containing the term `bitcoin`.
```
roClient.v2.search('bitcoin', { 'media.fields': 'url' });
```
The response provided is as such:
- The reponse is in the form of ![output_1](/assets/images/readme/output_getTopicWiseTweets().png)
- We can see the format of the JSON response and the associated tokenised list.
- Sentiment Analysis is performed on the value of the "text" key.

2. *getUserTweets()*
- Uses the `v2.userTimeline()` function from the `roClient` to call the Twitter REST api to retrieve tweets from a specific User's timeline.
- In the example given below, we retrieve the recent tweets from *@cz_binance* using the following parameters:
```
roClient.v2.userTimeline('902926941413453824', { exclude: 'replies' });
```
![output_2](/assets/images/readme/output_getUserTweets().png)
- Sentiment Analysis is performed on the value of the "text" key.

3. *getNewsArticles*
- Uses the `v2.everything()` function from the `newsClient` to call the NewsAPI to retrieve the relevant news artcles specified by a parameter *`q`*.
- The parameters are:
```
v2.everything({
        q: 'bitcoin',
        // sources: 'bbc-news,coindesk,cointelegraph',
        // domains: 'bbc.co.uk,coindesk.com,cointelegraph.com',
        from: '2022-06-29',
        to: '2022-06-30',
        language: 'en',
        sortBy: 'relevancy',
        page: 1
    })
```
- The requisite reponse JSON has been parsed to 
![output_3](/assets/images/readme/output_getNewsArticles().png)

## References
